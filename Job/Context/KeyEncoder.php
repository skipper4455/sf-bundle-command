<?php

namespace TCS\CommandBundle\Job\Context;

use TCS\CommandBundle\Entity\Job;

class KeyEncoder
{

    const SEPARATOR = ':';

    /**
     * @return KeyEncoder
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @param Job $job
     * @return string
     */
    public function createKey(Job $job)
    {
        return $this->encode(
            $job->getId(),
            md5(microtime(true) . $job->getCommandName() . mt_rand(0, 999))
        );
    }

    /**
     * @param int $jobId
     * @param $key
     * @return string
     */
    public function encode($jobId, $key)
    {
        return $jobId.static::SEPARATOR.$key;
    }

    /**
     * @param string $contextKey
     * @return array
     */
    public function decode($contextKey)
    {
        return explode(static::SEPARATOR, $contextKey);
    }
}