<?php

namespace TCS\CommandBundle\Job\Context;

final class Events
{
    /**
     * A job is started
     */
    const START = 'start';

    /**
     * A job has ended
     */
    const END = 'end';

    /**
     * A job notified some progress
     */
    const PROGRESS = 'progress';
}