<?php

namespace TCS\CommandBundle\Job;

use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Filesystem\Filesystem;

class OutputFactory
{
    /**
     * @var string
     */
    private $basePath;

    public function __construct($basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * @param string $key
     * @return StreamOutput
     */
    public function create($key)
    {
        return new StreamOutput($this->openOutputStream($key));
    }

    /**
     * @param string $key
     * @return resource
     */
    private function openOutputStream($key)
    {
        $fs = new Filesystem();

        $filepath = implode('/', [
            rtrim($this->basePath, '/'),
            substr($key, 0, 2),
            substr($key, 2, 2),
            $key . '.log',
        ]);

        if (!$fs->exists(dirname($filepath))) {
            $fs->mkdir(dirname($filepath));
        }

        return @fopen($filepath, 'w');
    }
}