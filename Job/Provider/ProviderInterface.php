<?php

namespace TCS\CommandBundle\Job\Provider;

use TCS\CommandBundle\Entity\Job;

interface ProviderInterface
{
    /**
     * Returns all scheduled job
     * @return Job[]
     */
    public function getScheduled();

    /**
     * @param int $id
     * @return Job mixed
     */
    public function find($id);
}