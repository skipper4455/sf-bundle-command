<?php

namespace TCS\CommandBundle\Job;

use TCS\CommandBundle\Entity\Job;

interface RunnerInterface
{
    /**
     * Run the defined command (with defined parameters) from the job instance
     * @param Job $job
     * @return mixed
     */
    public function run(Job $job);
}