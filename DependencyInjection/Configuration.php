<?php

namespace TCS\CommandBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @var string
     */
    private $projectDir;

    public function __construct($projectDir)
    {
        $this->projectDir = $projectDir;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tcs_command');

        $this->addCrontabSection($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    public function addCrontabSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('crontab')
                ->children()
                    ->scalarNode('path')
                        ->cannotBeEmpty()
                        ->defaultValue($this->projectDir)
                    ->end()
                    ->scalarNode('user')->end()
                    ->booleanNode('add_path')->defaultFalse()->end()
                    ->booleanNode('clear_output')->defaultTrue()->end()
                ->end()
            ->end();
    }
}
