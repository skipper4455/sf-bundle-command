<?php

namespace TCS\CommandBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class TCSCommandExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration($container->getParameter('kernel.project_dir'));
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $this->injectCrontabConfig($container, $config['crontab']);
    }

    /**
     * @param ContainerBuilder $container
     * @param array $config
     */
    private function injectCrontabConfig(ContainerBuilder $container, array $config)
    {
        $dumperDefinition = $container->findDefinition('tcs_command.crontab.dumper');

        if ($dumperDefinition) {
            $dumperDefinition->setArgument(1, $config);
        }
    }
}
