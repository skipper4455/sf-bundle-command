<?php

namespace TCS\CommandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Log
 * @package TCS\CommandBundle\Entity
 * @ORM\Entity(repositoryClass="TCS\CommandBundle\Entity\Repository\LogRepository")
 * @ORM\Table(name="tcs_log")
 */
class Log
{

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=32, name="key_id")
     * "key" is a reserved keyword in SQL and causes Doctrine to fail at inserting
     */
    protected $key;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="started_at", nullable=true)
     */
    protected $startedAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="ended_at", nullable=true)
     */
    protected $endedAt;

    /**
     * @var integer
     * @ORM\Column(type="integer", name="exit_code", nullable=true)
     */
    protected $exitCode;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $progress;

    /**
     * @var Job
     * @ORM\ManyToOne(targetEntity="TCS\CommandBundle\Entity\Job", inversedBy="history")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $job;

    /**
     * Log constructor.
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = $key;
        $this->progress = 0;
    }

    /**
     * @return int
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime $startedAt
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return \DateTime
     */
    public function getEndedAt()
    {
        return $this->endedAt;
    }

    /**
     * @param \DateTime $endedAt
     */
    public function setEndedAt($endedAt)
    {
        $this->endedAt = $endedAt;
    }

    /**
     * @return int
     */
    public function getExitCode()
    {
        return $this->exitCode;
    }

    /**
     * @param int $exitCode
     */
    public function setExitCode($exitCode)
    {
        $this->exitCode = $exitCode;
    }

    /**
     * @return float
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param float $progress
     */
    public function setProgress($progress)
    {
        $this->progress = max(0, min($progress, 1));
    }

    /**
     * @return bool
     */
    public function isRunning()
    {
        return $this->endedAt === null || $this->exitCode === null;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Job $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }

}