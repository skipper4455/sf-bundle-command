<?php

namespace TCS\CommandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TCS\CommandBundle\Crontab\Period\DayOfMonthPeriod;
use TCS\CommandBundle\Crontab\Period\DayOfWeekPeriod;
use TCS\CommandBundle\Crontab\Period\HourPeriod;
use TCS\CommandBundle\Crontab\Period\MinutePeriod;
use TCS\CommandBundle\Crontab\Period\MonthPeriod;

/**
 * Class Schedule
 * @package TCS\CommandBundle\Command
 * @ORM\Entity(repositoryClass="TCS\CommandBundle\Entity\Repository\ScheduleRepository")
 * @ORM\Table(name="tcs_schedule")
 */
class Schedule
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var MinutePeriod
     * @ORM\Column(type="object")
     */
    protected $minutes;

    /**
     * @var HourPeriod
     * @ORM\Column(type="object")
     */
    protected $hours;

    /**
     * @var DayOfMonthPeriod
     * @ORM\Column(type="object", name="days_of_month")
     */
    protected $daysOfMonth;

    /**
     * @var MonthPeriod
     * @ORM\Column(type="object")
     */
    protected $months;

    /**
     * @var DayOfWeekPeriod
     * @ORM\Column(type="object", name="days_of_week")
     */
    protected $daysOfWeek;

    /**
     * @var Job
     * @ORM\OneToOne(targetEntity="TCS\CommandBundle\Entity\Job", inversedBy="schedule")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $job;

    /**
     * Schedule constructor.
     */
    public function __construct()
    {
        $this->minutes = [];
        $this->hours = [];
        $this->daysOfMonth = [];
        $this->months = [];
        $this->daysOfWeek = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return MinutePeriod
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * @param MinutePeriod $minutes
     * @return $this
     */
    public function setMinutes(MinutePeriod $minutes)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * @return HourPeriod
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @param HourPeriod $hours
     * @return $this
     */
    public function setHours(HourPeriod $hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * @return DayOfMonthPeriod
     */
    public function getDaysOfMonth()
    {
        return $this->daysOfMonth;
    }

    /**
     * @param DayOfMonthPeriod $daysOfMonth
     * @return $this
     */
    public function setDaysOfMonth(DayOfMonthPeriod $daysOfMonth)
    {
        $this->daysOfMonth = $daysOfMonth;

        return $this;
    }

    /**
     * @return MonthPeriod
     */
    public function getMonths()
    {
        return $this->months;
    }

    /**
     * @param MonthPeriod $months
     * @return $this
     */
    public function setMonths(MonthPeriod $months)
    {
        $this->months = $months;

        return $this;
    }

    /**
     * @return DayOfWeekPeriod
     */
    public function getDaysOfWeek()
    {
        return $this->daysOfWeek;
    }

    /**
     * @param DayOfWeekPeriod $daysOfWeek
     * @return $this
     */
    public function setDaysOfWeek(DayOfWeekPeriod $daysOfWeek)
    {
        $this->daysOfWeek = $daysOfWeek;

        return $this;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param $job
     * @return $this
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }
}