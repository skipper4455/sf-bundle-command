<?php

namespace TCS\CommandBundle;

use Symfony\Component\Console\Application;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class TCSCommandBundle extends Bundle
{
    /**
     * @param Application $application
     */
    public function registerCommands(Application $application)
    {
        // we don't want this bundle to auto-register any commands
    }
}