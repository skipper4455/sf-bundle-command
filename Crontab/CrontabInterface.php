<?php

namespace TCS\CommandBundle\Crontab;

interface CrontabInterface
{

    /**
     * Refresh the installed crontab based on registered scheduled jobs
     * @return void
     */
    public function refresh();

    /**
     * Returns whether the installed crontab is stale
     * @return boolean
     */
    public function isStale();

}