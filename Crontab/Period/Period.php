<?php

namespace TCS\CommandBundle\Crontab\Period;

abstract class Period implements \Serializable
{
    const FORMAT_RANGE = '-';
    const FORMAT_LIST = ',';
    const FORMAT_STEP = '/';
    const FORMAT_ALL = '*';

    const TYPE_ALL = 0;
    const TYPE_LIST = 1;
    const TYPE_RANGE = 2;

    const DEFAULT_STEP = 1;

    /**
     * @var int
     */
    protected $type;

    /**
     * @var int[]
     */
    protected $values;

    /**
     * @var int
     */
    protected $step;

    /**
     * @param $value
     * @return mixed
     */
    abstract protected function normalizeValue($value);

    /**
     * Period constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param null $step
     * @return static
     */
    public static function all($step = null)
    {
        $period = new static();

        $period->type = static::TYPE_ALL;
        $period->values = [];
        $period->step = $step ?: static::DEFAULT_STEP;

        return $period;
    }

    /**
     * @param array $values
     * @return static
     */
    public static function fromList(array $values = [])
    {
        $period = new static();

        $period->type = static::TYPE_LIST;
        foreach ($values as $value) {
            $period->values[] = $period->normalizeValue($value);
        }
        $period->step = static::DEFAULT_STEP;

        return $period;
    }

    /**
     * @param $start
     * @param $end
     * @param null $step
     * @return static
     */
    public static function fromRange($start, $end, $step = null)
    {
        $period = new static();

        $period->type = static::TYPE_RANGE;
        $period->values = [
            $period->normalizeValue($start),
            $period->normalizeValue($end),
        ];

        sort($period->values);

        $period->step = $step ?: static::DEFAULT_STEP;

        return $period;
    }

    /**
     *
     */
    public function toString()
    {
        switch ($this->type) {
            default:
            case static::TYPE_ALL:
                return static::FORMAT_ALL .
                ($this->step !== static::DEFAULT_STEP ? static::FORMAT_STEP . $this->step : '');

            case static::TYPE_LIST:
                return implode(static::FORMAT_LIST, $this->values);

            case static::TYPE_RANGE:
                return implode(static::FORMAT_RANGE, $this->values) .
                ($this->step !== static::DEFAULT_STEP ? static::FORMAT_STEP . $this->step : '');
        }
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return $this->toString();
    }

    /**
     * @param string $serialized (is considered safe)
     */
    public function unserialize($serialized)
    {
        $this->step = static::DEFAULT_STEP;

        // case ALL. Ex: */5
        if (strpos($serialized, static::FORMAT_ALL) !== false) {
            $this->type = static::TYPE_ALL;

            if (strpos($serialized, static::FORMAT_STEP) !== false) {
                $this->step = (int)substr($serialized, strpos($serialized, static::FORMAT_STEP) + 1);
            }

        // case RANGE. Ex: 1-12/5
        } elseif (preg_match(
            '/(\d+)' . static::FORMAT_RANGE . '(\d+)(\\' . static::FORMAT_STEP . '(\d+))?/',
            $serialized,
            $matches
        )) {
            $this->type = static::TYPE_RANGE;
            $this->values = [(int)$matches[1], (int)$matches[2]];

            if (!empty($matches[4])) {
                $this->step = (int)$matches[4];
            }

        // case LIST. Ex : 1,5,6,9,21
        } elseif (strpos($serialized, static::FORMAT_LIST) !== false) {
            $this->type = static::TYPE_LIST;
            $this->values = array_map('intval', explode(static::FORMAT_LIST, $serialized));
        }
    }


}