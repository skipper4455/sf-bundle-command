<?php

namespace TCS\CommandBundle\Crontab\Period;

class DayOfWeekPeriod extends Period
{

    /**
     * @param $value
     * @return bool
     */
    protected function normalizeValue($value)
    {
        return max(0, min(6, (int)$value));
    }

}