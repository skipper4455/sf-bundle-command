<?php

namespace TCS\CommandBundle\Crontab\Period;

class MinutePeriod extends Period
{

    /**
     * @param $value
     * @return bool
     */
    protected function normalizeValue($value)
    {
        return max(0, min(59, (int)$value));
    }

}