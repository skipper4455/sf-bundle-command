<?php

namespace TCS\CommandBundle\Crontab;

use TCS\CommandBundle\Entity\Job;

interface DumperInterface
{
    /**
     * Dump the crontab string corresponding to the passed job array
     * @param Job[] $jobs
     * @return string
     */
    public function dump(array $jobs = []);
}