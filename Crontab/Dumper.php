<?php

namespace TCS\CommandBundle\Crontab;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Process\PhpExecutableFinder;
use TCS\CommandBundle\Entity\Job;

final class Dumper implements DumperInterface
{
    /**
     * @var Command
     */
    private $runner;

    /**
     * @var string[]
     */
    private $options;

    public function __construct(Command $runner, array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->runner = $runner;

        $this->options = $resolver->resolve($options);
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    private function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'user'         => null,
            'add_path'     => false,
            'clear_output' => true,
        ]);
        $resolver->setRequired(['path']);
        $resolver->setAllowedTypes('add_path', ['boolean']);
        $resolver->setAllowedTypes('clear_output', ['boolean']);
        $resolver->setNormalizer('path', function (Options $options, $path) {
            return realpath($path) ?: '';
        });
    }

    /**
     * @inheritdoc
     */
    public function dump(array $jobs = [])
    {
        $tab = [];

        foreach ($jobs as $job) {
            $tab[$job->getId()] = $this->dumpJob($job);
        }

        // providing consistency for further comparison
        ksort($tab);

        return implode("\n", $tab);
    }

    /**
     * @param Job $job
     * @return string
     */
    protected function dumpJob(Job $job)
    {
        return implode(' ', [
            $job->getSchedule()->getMinutes()->toString(),
            $job->getSchedule()->getHours()->toString(),
            $job->getSchedule()->getDaysOfMonth()->toString(),
            $job->getSchedule()->getMonths()->toString(),
            $job->getSchedule()->getDaysOfWeek()->toString(),
            $this->getCommandLine($job),
        ]);
    }

    /**
     * @param Job $job
     * @return string
     */
    private function getCommandLine(Job $job)
    {
        // php bin/console <run-command> <job-id> > /dev/null 2>&1
        $commandLineParts = [];

        if ($this->options['add_path']) {
            $commandLineParts[] = 'cd';
            $commandLineParts[] = $this->options['path'];
            $commandLineParts[] = '&&';
        }

        if ($this->options['user']) {
            $commandLineParts[] = 'sudo';
            $commandLineParts[] = '-u';
            $commandLineParts[] = $this->options['user'];
        }

        $commandLineParts[] = $this->findPhpBinary();
        $commandLineParts[] = $this->findConsoleScript();
        $commandLineParts[] = $this->runner->getName();
        $commandLineParts[] = $job->getId();

        if ($this->options['clear_output']) {
            $commandLineParts[] = '>/dev/null';
            $commandLineParts[] = '2>&1';
        }

        return implode(' ', $commandLineParts);
    }

    /**
     * @return false|string
     */
    private function findPhpBinary()
    {
        return (new PhpExecutableFinder())->find();
    }

    /**
     * @return string
     */
    private function findConsoleScript()
    {
        $path = '';

        if (!$this->options['add_path']) {
            $path = $this->options['path'] . '/';
        }

        $path .= Kernel::MAJOR_VERSION < 3 ? 'app' : 'bin';

        $path .= '/console';

        return $path;
    }
}