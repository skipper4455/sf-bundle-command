<?php

namespace TCS\CommandBundle\Crontab\Exception;

class CrontabRefreshException extends \RuntimeException
{
}