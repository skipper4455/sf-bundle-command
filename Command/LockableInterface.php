<?php

namespace TCS\CommandBundle\Command;

interface LockableInterface
{
    /**
     * Returns the lock name
     * @return string
     */
    public function getLockName();
}