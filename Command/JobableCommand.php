<?php

namespace TCS\CommandBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use TCS\CommandBundle\Job\Context\Context;
use TCS\CommandBundle\Job\Context\KeyEncoder;

abstract class JobableCommand extends Command implements JobableInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const JOBCONTEXTKEY_PARAMETER = '__jck';

    /**
     * @var Context
     */
    private $context;

    private $jobContextKeyOptionDefinitionMerged;

    /**
     * @inheritdoc
     */
    public function isExposed()
    {
        return true;
    }

    /**
     * @return Locker
     */
    protected function getLocker()
    {
        return $this->container->get('tcs_command.locker');
    }

    /**
     * @inheritdoc
     */
    public function getLockName()
    {
        return str_replace(['.', ':'], '_', $this->getName()) . '.lock';
    }

    /**
     * @param bool $mergeArgs
     * @return \Symfony\Component\Console\Input\InputDefinition
     */
    public function mergeApplicationDefinition($mergeArgs = true)
    {
        if (true !== $this->jobContextKeyOptionDefinitionMerged) {
            $definition = $this->getDefinition();
            $definition->addOption(new InputOption(
                static::JOBCONTEXTKEY_PARAMETER,
                null,
                InputOption::VALUE_OPTIONAL
            ));

            $this->setDefinition($definition);
            $this->jobContextKeyOptionDefinitionMerged = true;
        }

        parent::mergeApplicationDefinition($mergeArgs);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Symfony\Component\Console\Exception\ExceptionInterface
     */
    public function run(InputInterface $input, OutputInterface $output)
    {
        $this->mergeApplicationDefinition();
        $input->bind($this->getDefinition());

        if ($key = $input->getOption(static::JOBCONTEXTKEY_PARAMETER)) {
            $this->createJobContextFromKey($key);
            $input->setOption(static::JOBCONTEXTKEY_PARAMETER, null);
        }

        if (!$this->getLocker()->acquire($this)) {
            $output->writeln('Command already running');

            return 0;
        }

        $statusCode = 1;

        try {
            $this->getContext()->start();
            $statusCode = parent::run($input, $output);
        } catch (\Exception $e) {
            // TODO : do better than that
            $output->write('<error>' . $e->getMessage() . '</error>');
        } finally {
            $this->getLocker()->release($this);
        }

        $this->getContext()->end($statusCode);

        return $statusCode;
    }

    /**
     * @param $contextKey
     * @return Context
     */
    protected function createJobContextFromKey($contextKey)
    {
        $this->context = $this
            ->container
            ->get('tcs_command.context_factory')
            ->createFromKey($contextKey);
    }

    /**
     * @return Context
     */
    protected function getContext()
    {
        return $this->context;
    }

}