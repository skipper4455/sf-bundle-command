<?php

namespace TCS\CommandBundle\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpKernel\Kernel;

class Registry implements RegistryInterface
{
    /**
     * @var Kernel
     */
    protected $kernel;

    /**
     * @var JobableInterface[]
     */
    protected $commands;

    /**
     * @var bool
     */
    protected $isRegistered;

    /**
     * Registry constructor.
     * @param Kernel $kernel
     */
    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
        $this->commands = [];
        $this->isRegistered = false;
    }

    /**
     *
     */
    protected function register()
    {
        if (!$this->isRegistered) {
            $application = new Application($this->kernel);

            foreach ($application->all() as $command) {
                if ($command instanceof JobableInterface) {
                    $this->commands[$command->getName()] = $command;
                }
            }

            $this->isRegistered = true;
        }
    }

    /**
     * @inheritdoc
     */
    public function all()
    {
        $this->register();

        return $this->commands;
    }

    /**
     * @inheritdoc
     */
    public function get($name)
    {
        $this->register();

        if (empty($this->commands[$name])) {
            throw new \InvalidArgumentException(sprintf(
                'The "%s" command does not exist or does not implement %s',
                $name,
                JobableInterface::class
            ));
        }

        $command = $this->commands[$name];

        if ($command instanceof ContainerAwareInterface) {
            $command->setContainer($this->kernel->getContainer());
        }

        return $command;
    }
}