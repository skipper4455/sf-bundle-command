<?php

namespace TCS\CommandBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TCS\CommandBundle\Crontab\CrontabInterface;
use TCS\CommandBundle\Crontab\Exception\CrontabRefreshException;

class RefreshCrontabCommand extends Command
{
    /**
     * @var CrontabInterface
     */
    private $crontab;

    public function __construct(CrontabInterface $crontab)
    {
        $this->crontab = $crontab;

        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('tcs:crontab:refresh')
            ->setDescription('Refreshes the crontab based on the scheduled jobs')
            ->addOption('force', 'f', InputOption::VALUE_NONE,
                'Causes the crontab to be refreshed even if it is not detected as stale');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if (!$this->crontab->isStale() && !$input->getOption('force')) {
            $io->writeln('<info>Crontab is already up-to-date.</info>');

            return 0;
        }

        try {
            $this->crontab->refresh();

            $io->success('Crontab successfully updated.');
        } catch (CrontabRefreshException $e) {
            $io->error('Cannot update the crontab : '.$e->getMessage());

            return 1;
        }

        return 0;
    }
}